
bool sem=true;
inline wait() { // macro definition
    atomic {
printf("sem: %d \n", sem);
    if
    ::sem==true ->
        sem=false;
        
    fi
    }
}
inline signal() {
    sem=true
}

active[11] proctype P() {
do
:: printf("Non-critical section P, %d \n", _pid);
atomic{
wait();
printf("Critical section P, %d \n", _pid);
signal()
printf("Finished-critical section P, %d \n", _pid);}
od
}
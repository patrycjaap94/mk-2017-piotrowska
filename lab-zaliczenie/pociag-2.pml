chan in = [0] of {bool};
chan out = [0] of {bool};

proctype Controller() {

	do
	::true ->
		in!false;
		out!false;
	od
}


active [40] proctype Train() {

	do
	:: true -> 
		printf("Pociag %d chce wjechac do tunelu\n",_pid);
		atomic{
		in?false;
		printf("Pociag nr %d jest w tunelu\n", _pid);}
		cs: skip;  
		atomic{
		out?false;	
		printf("Pociag nr %d wyjechał z tunelu\n", _pid);}

	od

}
init{
	run Controller();
}

ltl mutex {[]!(Train[0]@cs && Train[1]@cs)}

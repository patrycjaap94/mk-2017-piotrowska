#define N 100
init {

    do
        :: int i=1;
    do
        :: break
        :: i<N -> i++
    od;

    printf("%d\n",i);

    assert(i!=100);

    od;

}
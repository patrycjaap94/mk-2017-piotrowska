#define N 5
byte n=0;

chan forks [N] = [0] of { byte }
chan jadalnia = [0] of { bool }

proctype Fork(chan ch) {

byte id;

    do 
    
        :: 	atomic{
                ch ? id;
                printf("Filozof %d podniosl widelec \n",id);
            }
            atomic{
    		    ch ? false;
                printf("Filozof %d odlozyl widelec \n", id); 
            }
    od

}


proctype Butler() {
    
    do 
        :: 	n<N-1 -> atomic{
		jadalnia ? true;
		n=n+1;
		printf("Ilosc filozofow w jadalini: %d\n", n);
		}

    	::	n>0 -> atomic{
		jadalnia ? false;
		n=n-1;
		printf("Ilosc filozofow w jadalini: %d\n", n);
		}


    od

}

proctype Philosopher(byte id; chan left; chan right) {

    do

        :: 
	    jadalnia ! true;

           /* printf("Filozof %d mysli\n", id);
*/

    atomic{
        left ! id;
        /*printf("Filozof %d podniosl lewy widelec\n", id);
*/
    }

    /*cs: skip;*/

    atomic{
        right ! id;/*
        printf("Filozof %d podniosl prawy widelec\n", id);
        printf("Filozof %d je\n", id);
*/
    }

    /*cs: skip;*/

    atomic{
        right ! false;
      /*  printf("Filozof %d przestaje jesc\n", id);
        printf("Filozof %d odklada prawy widelec\n", id);
*/
    }

    /*cs: skip;*/

    atomic{
        left ! false;
  /*      printf("Filozof %d odklada lewy widelec\n", id);
*/
    }

jadalnia ! false;

    od


}

init{
int i;
atomic{
	for (i : 0 .. N-1) {
		run Fork(forks[i]);
	}
	}
    run Butler();
i=1;
    	for (i : 1 .. N) {
    	if
    	:: i<N ->
    		run Philosopher(i-1, forks[i-1], forks[i]);
    		printf("%d,\n",j-1);
    	fi

		}
		    run Philosopher(N, forks[N], forks[0]);
}


ltl a {[](n<N)}

#define N 5
chan forks [N] = [0] of { bool }

proctype Fork(chan ch) {

    do
        :: 	ch ! true;
    		ch ! false;

    od

}


proctype Filozof(byte id; chan left; chan right) {

    do
        :: 

    atomic{
        left ? true;
        printf("Filozof %d podniosl lewy widelec\n", id);

    }

    /*cs: skip;*/

    atomic{
        right ? true;
        printf("Filozof %d podniosl prawy widelec\n", id);
        printf("Filozof %d je\n", id);

    }

    /*cs: skip;*/

    atomic{
        right ? false;
     //   printf("Filozof %d przestaje jesc\n", id);
     //   printf("Filozof %d odklada prawy widelec\n", id);

    }

    /*cs: skip;*/

    atomic{
        left ? false;
        printf("Filozof %d odklada lewy widelec\n", id);

    }

    od
}

init{
    run Fork(forks[0]);
    run Fork(forks[1]);
    run Fork(forks[2]);
    run Fork(forks[3]);
    run Fork(forks[4]);
    run Filozof(0, forks[0], forks[1]);
    run Filozof(1, forks[1], forks[2]);
    run Filozof(2, forks[2], forks[3]);
    run Filozof(3, forks[3], forks[4]);
    run Filozof(4, forks[4], forks[0]);
}
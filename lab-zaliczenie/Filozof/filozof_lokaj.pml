#define N 5
chan forks [5] = [0] of { bool }
chan jadalnia = [0] of { bool }

proctype Fork(chan ch) {

    do 
        :: 	ch ! true; 
    		ch ! false;

    od

}

proctype Butler() {
    byte n=0;
    do 
        :: 	n<N-1 -> atomic{
		jadalnia ? true;
		n=n+1;
		printf("Ilosc filozofow:  %d\n", n);
		}

	::	n>0 -> atomic{
		jadalnia ? false;
		n=n-1;
		printf("Ilosc filozofow : %d\n", n);
		}


    od

}

proctype Philosopher(byte id; chan left; chan right) {

    do

        :: 
	    jadalnia ! true;

            printf("Filozof %d mysli\n", id);


    atomic{
        left ? true;
        printf("Filozof %d podniosl lewy widelec\n", id);

    }

    /*cs: skip;*/

    atomic{
        right ? true;
        printf("Filozof %d podniosl prawy widelec\n", id);
        printf("Filozof %d je\n", id);

    }

    /*cs: skip;*/

    atomic{
        right ? false;
        printf("Filozof %d przestaje jesc\n", id);
        printf("Filozof %d odklada prawy widelec\n", id);

    }

    /*cs: skip;*/

    atomic{
        left ? false;
        printf("Filozof %d odklada lewy widelec\n", id);

    }

jadalnia ! false;

    od


}

init{
    run Fork(forks[0]);
    run Fork(forks[1]);
    run Fork(forks[2]);
    run Fork(forks[3]);
    run Fork(forks[4]);
    run Butler();
    run Philosopher(0, forks[0], forks[1]);
    run Philosopher(1, forks[1], forks[2]);
    run Philosopher(2, forks[2], forks[3]);
    run Philosopher(3, forks[3], forks[4]);
    run Philosopher(4, forks[4], forks[0]);
}
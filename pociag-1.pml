chan in = [0] of {bool};
chan out = [0] of {bool};

active proctype Controller() {

	do
	::true ->
		in!false;
		out!false;
	od
}


active proctype Train1() {

	do
	:: true -> 
		printf("Pociag 1 chce wjechac do tunelu\n");
		atomic{
		in?false;
		printf("Pociag nr 1 jest w tunelu\n");}
		cs: skip;  
		atomic{
		out?false;	
		printf("Pociag nr 1 wyjechał z tunelu\n");}

	od

}

active proctype Train2() {

	do
	:: true -> 
		printf("Pociag 2 chce wjechac do tunelu\n");
		atomic{
		in?false;
		printf("Pociag nr 2 jest w tunelu\n");}
		cs: skip;  
		atomic{
		out?false;	
		printf("Pociag nr 2 wyjechał z tunelu\n");}

	od

}

ltl mutex { []!(Train1@cs && Train2@cs) }

/* Copyright 2007-10 by Moti Ben-Ari under the GNU GPL; see readme.txt */

bool wantP = false, wantQ = false;
bool csp = false;
bool csq = false;

ltl { <>csp }

active proctype P() {
        do
        :: wantP = true;
		   !wantQ;
		   csp=true;
		   csp=false;
		   wantP=false;
	od
}

active proctype Q() {	
do
        :: wantP = true;
		   !wantP;
		   csq=true;
		   csq=false;
		   wantQ=false;
	od
	

	
}

ltl mutex { []!(csp && csq) }
ltl nostarvation { []<>csp }

bool    wantP, wantQ;
int critical =0;

active proctype P() {
do
:: wantP = true;
!wantQ;
critical++;
critical--;
wantP = false;
od
}


/*Similarly for process Q*/

active proctype Q() {
do
:: wantQ = true;
!wantP;
critical++;
critical--;
wantQ = false;
od
}

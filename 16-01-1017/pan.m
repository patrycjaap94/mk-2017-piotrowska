#define rand	pan_rand
#define pthread_equal(a,b)	((a)==(b))
#if defined(HAS_CODE) && defined(VERBOSE)
	#ifdef BFS_PAR
		bfs_printf("Pr: %d Tr: %d\n", II, t->forw);
	#else
		cpu_printf("Pr: %d Tr: %d\n", II, t->forw);
	#endif
#endif
	switch (t->forw) {
	default: Uerror("bad forward move");
	case 0:	/* if without executable clauses */
		continue;
	case 1: /* generic 'goto' or 'skip' */
		IfNotBlocked
		_m = 3; goto P999;
	case 2: /* generic 'else' */
		IfNotBlocked
		if (trpt->o_pm&1) continue;
		_m = 3; goto P999;

		 /* CLAIM mutex */
	case 3: // STATE 1 - _spin_nvr.tmp:3 - [(!(!(((Train[0]._p==cs)&&(Train[1]._p==cs)))))] (6:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported1 = 0;
			if (verbose && !reported1)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported1 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[3][1] = 1;
		if (!( !( !(((((int)((P1 *)Pptr(BASE+0))->_p)==6)&&(((int)((P1 *)Pptr(BASE+1))->_p)==6))))))
			continue;
		/* merge: assert(!(!(!(((Train[0]._p==cs)&&(Train[1]._p==cs))))))(0, 2, 6) */
		reached[3][2] = 1;
		spin_assert( !( !( !(((((int)((P1 *)Pptr(BASE+0))->_p)==6)&&(((int)((P1 *)Pptr(BASE+1))->_p)==6))))), " !( !( !(((Train[0]._p==cs)&&(Train[1]._p==cs)))))", II, tt, t);
		/* merge: .(goto)(0, 7, 6) */
		reached[3][7] = 1;
		;
		_m = 3; goto P999; /* 2 */
	case 4: // STATE 10 - _spin_nvr.tmp:8 - [-end-] (0:0:0 - 1)
		
#if defined(VERI) && !defined(NP)
#if NCLAIMS>1
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	int nn = (int) ((Pclaim *)pptr(0))->_n;
				printf("depth %ld: Claim %s (%d), state %d (line %d)\n",
					depth, procname[spin_c_typ[nn]], nn, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#else
		{	static int reported10 = 0;
			if (verbose && !reported10)
			{	printf("depth %d: Claim, state %d (line %d)\n",
					(int) depth, (int) ((Pclaim *)pptr(0))->_p, src_claim[ (int) ((Pclaim *)pptr(0))->_p ]);
				reported10 = 1;
				fflush(stdout);
		}	}
#endif
#endif
		reached[3][10] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC :init: */
	case 5: // STATE 1 - pociag2.pml:42 - [(run Controller())] (0:0:0 - 1)
		IfNotBlocked
		reached[2][1] = 1;
		if (!(addproc(II, 1, 0, 0)))
			continue;
		_m = 3; goto P999; /* 0 */
	case 6: // STATE 2 - pociag2.pml:45 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[2][2] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Train */
	case 7: // STATE 1 - pociag2.pml:25 - [(1)] (5:0:0 - 1)
		IfNotBlocked
		reached[1][1] = 1;
		if (!(1))
			continue;
		/* merge: printf('pociag nr %d chce wjechac do tunelu\\n',_pid)(0, 2, 5) */
		reached[1][2] = 1;
		Printf("pociag nr %d chce wjechac do tunelu\n", ((int)((P1 *)this)->_pid));
		_m = 3; goto P999; /* 1 */
	case 8: // STATE 3 - pociag2.pml:28 - [in?0] (6:0:0 - 1)
		reached[1][3] = 1;
		if (boq != now.in) continue;
		if (q_len(now.in) == 0) continue;

		XX=1;
		if (0 != qrecv(now.in, 0, 0, 0)) continue;
		
#ifndef BFS_PAR
		if (q_flds[((Q0 *)qptr(now.in-1))->_t] != 1)
			Uerror("wrong nr of msg fields in rcv");
#endif
		;
		qrecv(now.in, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.in);
		sprintf(simtmp, "%d", 0); strcat(simvals, simtmp);		}
#endif
		if (q_zero(now.in))
		{	boq = -1;
#ifndef NOFAIR
			if (fairness
			&& !(trpt->o_pm&32)
			&& (now._a_t&2)
			&&  now._cnt[now._a_t&1] == II+2)
			{	now._cnt[now._a_t&1] -= 1;
#ifdef VERI
				if (II == 1)
					now._cnt[now._a_t&1] = 1;
#endif
#ifdef DEBUG
			printf("%3d: proc %d fairness ", depth, II);
			printf("Rule 2: --cnt to %d (%d)\n",
				now._cnt[now._a_t&1], now._a_t);
#endif
				trpt->o_pm |= (32|64);
			}
#endif

		};
		/* merge: printf('pociag nr %d  w tunelu\\n',_pid)(0, 4, 6) */
		reached[1][4] = 1;
		Printf("pociag nr %d  w tunelu\n", ((int)((P1 *)this)->_pid));
		_m = 4; goto P999; /* 1 */
	case 9: // STATE 7 - pociag2.pml:33 - [out?0] (10:0:0 - 1)
		reached[1][7] = 1;
		if (boq != now.out) continue;
		if (q_len(now.out) == 0) continue;

		XX=1;
		if (0 != qrecv(now.out, 0, 0, 0)) continue;
		
#ifndef BFS_PAR
		if (q_flds[((Q0 *)qptr(now.out-1))->_t] != 1)
			Uerror("wrong nr of msg fields in rcv");
#endif
		;
		qrecv(now.out, XX-1, 0, 1);
		
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[32];
			sprintf(simvals, "%d?", now.out);
		sprintf(simtmp, "%d", 0); strcat(simvals, simtmp);		}
#endif
		if (q_zero(now.out))
		{	boq = -1;
#ifndef NOFAIR
			if (fairness
			&& !(trpt->o_pm&32)
			&& (now._a_t&2)
			&&  now._cnt[now._a_t&1] == II+2)
			{	now._cnt[now._a_t&1] -= 1;
#ifdef VERI
				if (II == 1)
					now._cnt[now._a_t&1] = 1;
#endif
#ifdef DEBUG
			printf("%3d: proc %d fairness ", depth, II);
			printf("Rule 2: --cnt to %d (%d)\n",
				now._cnt[now._a_t&1], now._a_t);
#endif
				trpt->o_pm |= (32|64);
			}
#endif

		};
		/* merge: printf('pociag nr %d wyjechal z tunelu\\n',_pid)(0, 8, 10) */
		reached[1][8] = 1;
		Printf("pociag nr %d wyjechal z tunelu\n", ((int)((P1 *)this)->_pid));
		/* merge: .(goto)(0, 11, 10) */
		reached[1][11] = 1;
		;
		_m = 4; goto P999; /* 2 */
	case 10: // STATE 13 - pociag2.pml:36 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[1][13] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */

		 /* PROC Controller */
	case 11: // STATE 2 - pociag2.pml:9 - [in!0] (0:0:0 - 1)
		IfNotBlocked
		reached[0][2] = 1;
		if (q_len(now.in))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.in);
		sprintf(simtmp, "%d", 0); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.in, 0, 0, 1);
		{ boq = now.in; };
		_m = 2; goto P999; /* 0 */
	case 12: // STATE 3 - pociag2.pml:10 - [out!0] (0:0:0 - 1)
		IfNotBlocked
		reached[0][3] = 1;
		if (q_len(now.out))
			continue;
#ifdef HAS_CODE
		if (readtrail && gui) {
			char simtmp[64];
			sprintf(simvals, "%d!", now.out);
		sprintf(simtmp, "%d", 0); strcat(simvals, simtmp);		}
#endif
		
		qsend(now.out, 0, 0, 1);
		{ boq = now.out; };
		_m = 2; goto P999; /* 0 */
	case 13: // STATE 7 - pociag2.pml:18 - [-end-] (0:0:0 - 1)
		IfNotBlocked
		reached[0][7] = 1;
		if (!delproc(1, II)) continue;
		_m = 3; goto P999; /* 0 */
	case  _T5:	/* np_ */
		if (!((!(trpt->o_pm&4) && !(trpt->tau&128))))
			continue;
		/* else fall through */
	case  _T2:	/* true */
		_m = 3; goto P999;
#undef rand
	}


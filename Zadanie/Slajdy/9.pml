proctype P(byte c; int n) {
    printf("Executing process %c (%d)\n", c, n);
}

init {
    printf("Starting...\n");
    atomic{
        run P('A', 0);
        run P('B', 1);
    }
    printf("All process terminated\n")
}